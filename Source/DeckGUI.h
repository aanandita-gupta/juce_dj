/*
  ==============================================================================

    DeckGUI.h
    Created: 3 Mar 2020 11:48:39am
    Author:  Aanandita Gupta

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "DJAudioPlayer.h"

//==============================================================================
/*
*/
class DeckGUI    : public Component,
                   public Button::Listener,
                   public Slider::Listener
{
    public:
        DeckGUI(DJAudioPlayer* _djAudioPlayer);
        ~DeckGUI();

        void paint (Graphics&) override;
        void resized() override;
    
        void buttonClicked(Button* button) override;
        void sliderValueChanged (Slider *slider) override;
    
        Slider volumeSlider;

    private:
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (DeckGUI)
        TextButton playButton;
        TextButton pauseButton;
        TextButton stopButton;
        TextButton loadButton;
        Slider positionSlider;
        Slider dial;
            
        Label volumeLabel;
        Label positionLabel;
    
        DJAudioPlayer* djAudioPlayer;
};
