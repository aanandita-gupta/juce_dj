/*
  ==============================================================================

    DJAudioPlayer.h
    Created: 17 Feb 2020 10:47:52pm
    Author:  Aanandita Gupta

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

class DJAudioPlayer : public AudioSource  {
    public:
        DJAudioPlayer();
        ~DJAudioPlayer();
        void load(URL file);
        void play();
        void stop();
        void setGain(double gain);
        void setPosition(double posInSecs);
        void setPositionRelative(double pos);
    
        void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
        void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override;
        void releaseResources() override;
    
    private:
        AudioFormatManager formatManager;
        std::unique_ptr<AudioFormatReaderSource> readerSource;
        AudioTransportSource transportSource;
};
