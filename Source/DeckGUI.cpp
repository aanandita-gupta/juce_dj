/*
  ==============================================================================

    DeckGUI.cpp
    Created: 3 Mar 2020 11:48:39am
    Author:  Aanandita Gupta

  ==============================================================================
*/

#include <JuceHeader.h>
#include "DeckGUI.h"

//==============================================================================
DeckGUI::DeckGUI(DJAudioPlayer* _djAudioPlayer) : djAudioPlayer{_djAudioPlayer}
{
    playButton.addListener(this);
    pauseButton.addListener(this);
    stopButton.addListener(this);
    volumeSlider.addListener(this);
    positionSlider.addListener(this);
    loadButton.addListener(this);
    
    playButton.setButtonText("PLAY");
    pauseButton.setButtonText("PAUSE");
    stopButton.setButtonText("STOP");
    volumeLabel.setText("Volume", dontSendNotification);
    volumeSlider.setRange(0, 1);
    positionLabel.setText("Position", dontSendNotification);
    positionSlider.setRange(0,1);
    loadButton.setButtonText("LOAD");
    dial.setSliderStyle(Slider::Rotary);
    dial.setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
    
    addAndMakeVisible(playButton);
    addAndMakeVisible(pauseButton);
    addAndMakeVisible(stopButton);
    addAndMakeVisible(loadButton);
    addAndMakeVisible(volumeLabel);
    addAndMakeVisible(volumeSlider);
    addAndMakeVisible(positionLabel);
    addAndMakeVisible(positionSlider);
    addAndMakeVisible(dial);
}

DeckGUI::~DeckGUI()
{
}

void DeckGUI::paint (Graphics& g)
{
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));   // clear the background
}

void DeckGUI::resized()
{
    float rowH = getWidth() / 3;
    playButton.setBounds(0, 0, getWidth(), rowH);
    pauseButton.setBounds(0, rowH, getWidth(), rowH);
    stopButton.setBounds(0, rowH*2, getWidth(), rowH);
    volumeLabel.setBounds(0, rowH*3, getWidth(), rowH);
    volumeSlider.setBounds(0, rowH*3.5, getWidth(), rowH);
    positionLabel.setBounds(0, rowH*4.5, getWidth(), rowH);
    positionSlider.setBounds(0, rowH*5, getWidth(), rowH);
    loadButton.setBounds(0, rowH*6, getWidth(), rowH);
    dial.setBounds(50, rowH*7, getWidth()/3, getHeight()/3);
}

void DeckGUI::buttonClicked(Button* button)
{
    if (button == &playButton )
    {
        djAudioPlayer->setPosition(0);
        djAudioPlayer->play();
    }
    if(button == &pauseButton)
    {
        djAudioPlayer->stop();
    }
    if (button == &stopButton )
    {
        //resets the playback position to 0
        djAudioPlayer->setPosition(10);
        djAudioPlayer->stop();
    }
    if (button == &loadButton )
    {
        FileChooser chooser ("Select an audio file to play...");
        if (chooser.browseForFileToOpen())
        {
            URL audioURL = URL{chooser.getResult()};
            djAudioPlayer->load(audioURL);
        }
    }
}

void DeckGUI::sliderValueChanged(Slider* slider)
{
    if (slider == &volumeSlider)
    {
        djAudioPlayer->setGain(slider->getValue());
    }
    if (slider == &positionSlider)
    {
        djAudioPlayer->setPositionRelative(slider->getValue());
    }
}
